package lesson8

import (
	"fmt"
	"runtime"
	"time"
)

func printStats(mem runtime.MemStats) {
	runtime.ReadMemStats(&mem)
	fmt.Println("mem.Alloc:", mem.Alloc)
	fmt.Println("mem.TotalAlloc:", mem.TotalAlloc)
	fmt.Println("mem.HeapAlloc:", mem.HeapAlloc)
	fmt.Println("mem.NumGC:", mem.NumGC)
	fmt.Println("-----")
}

func RunGcCode() {
	var mem runtime.MemStats
	for i := 0; i < 2; i++ {
		s := make([]byte, 100000000)
		s2 := make([]byte, 100000000)
		if s == nil && s2 == nil{
			fmt.Println("Operation failed!")
		}
		printStats(mem)
	}
	time.Sleep(time.Second)
	// adding time.Sleep so that GC finishes it works and print out the output to terminal
}
