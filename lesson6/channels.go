package lesson6

import (
	"fmt"
	"time"
)

//func sender(channel chan<- string, message string) {
//	channel <- message
//}
//
//func receiver(receiver <-chan string, sender chan<- string) {
//	message := <-receiver
//	sender <- message
//}

func RunChannelCodes() {

	//ch := make(chan string, 3)
	//ch <- "naveen"
	//ch <- "paul"
	//ch <- "paul"
	//fmt.Println("capacity is", cap(ch))
	//fmt.Println("length is", len(ch))
	//fmt.Println("read value", <-ch)
	//fmt.Println("new length is", len(ch))

	//chan1 := make(chan string, 1)
	//chan2 := make(chan string, 1)
	//sender(chan1, "My name is Bond.James Bond.")
	//receiver(chan1, chan2)
	//fmt.Println(<-chan2)

	//messages := make(chan string, 2)
	//
	//messages <- "message 1"
	//messages <- "message 2"
	////messages <- "message 3"
	//
	//fmt.Println(<-messages)
	//fmt.Println(<-messages)



	//channelOne := make(chan string, 1)
	//channelTwo := make(chan string, 1)
	//channelEval := make(chan string, 1)
	//
	//go CalculateOne(channelOne)
	//go CalculateTwo(channelTwo)
	//go EvaluateTestData(channelEval)
	//
	//for i := 0; i < 3; i++ {
	//	select {
	//	case messageOne := <-channelOne:
	//		fmt.Println(messageOne)
	//	case messageTwo := <-channelTwo:
	//		fmt.Println(messageTwo)
	//	case messageEval := <-channelEval:
	//		fmt.Println(messageEval)
	//	}
	//}

	//c1 := make(chan string, 1)
	//go func() {
	//	time.Sleep(2 * time.Second)
	//	c1 <- "result 1"
	//}()
	//
	//select {
	//case res := <-c1:
	//	fmt.Println(res)
	//case <-time.After(3 * time.Second):
	//	fmt.Println("timeout c1")
	//}

	//messages := make(chan string, 1)
	//
	//select {
	//case msg := <-messages:
	//	fmt.Println("received message", msg)
	//default:
	//	fmt.Println("no activity")
	//}

	//jobs := make(chan int, 5)
	//done := make(chan bool)
	//
	//go func() {
	//	for {
	//		j, more := <-jobs
	//		if more {
	//			fmt.Println("received job", j)
	//		} else {
	//			fmt.Println("received all jobs")
	//			done <- true
	//			return
	//		}
	//	}
	//}()
	//
	//for j := 1; j <= 3; j++ {
	//	jobs <- j
	//	fmt.Println("sent job", j)
	//}
	//close(jobs)
	//fmt.Println("sent all jobs")
	//
	//<-done

	//queue := make(chan string, 2)
	//queue <- "one"
	//queue <- "two"
	//close(queue)
	//
	//for elem := range queue {
	//	fmt.Println(elem)
	//}
	//fmt.Println("range is over")

	//tick := time.Tick(1000 * time.Millisecond)
	//boom := time.After(5000 * time.Millisecond)
	//for {
	//	select {
	//	case <-tick:
	//		fmt.Println("tick.")
	//	case <-boom:
	//		fmt.Println("BOOM!")
	//		return
	//	default:
	//		fmt.Println("    .")
	//		time.Sleep(50 * time.Millisecond)
	//	}
	//}

	//runWgCode()

	fiboCode()
}



func CalculateOne(channel chan string){
	fmt.Println("Calculation phase one...")
	time.Sleep(time.Second*2)
	channel<-"phase one is done"
}

func CalculateTwo(channel chan string){
	fmt.Println("Calculation phase two...")
	time.Sleep(time.Second*5)
	channel<-"phase two is done"
}

func EvaluateTestData(channel chan string){
	fmt.Println("Creting test data...")
	time.Sleep(time.Second*3)
	channel<-"Evaluation is done"
}
