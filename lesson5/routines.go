package lesson5

import (
	"fmt"
	"time"
)

type sample struct {
	Do func()
}
func RunRoutinesCodes()  {
	//greeting()
	//go greeting()
	simpleChannel:=make(chan string)
	//go greeting2(simpleChannel)
	fmt.Println(<-simpleChannel)
	//go func() {
	//	fmt.Println("hello")
	//}()
	//time.Sleep(time.Second)
	fmt.Println("codes codes codes")
}

//func greeting()  {
//	fmt.Println("hello")
//}

func greeting2(hasan chan string)  {
	fmt.Println("hello")
	time.Sleep(time.Second * 5)
	hasan<-"hello channel"
}