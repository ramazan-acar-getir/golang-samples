package lesson5

import "fmt"

func RunDeferCodes(){
	defer fmt.Println("ilk defer")
	defer fmt.Println("ikinci defer")
	a := 2+2
	fmt.Println(a)
	fmt.Println("bazi islemler yapiliyor")
}
