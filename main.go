package main

import (
	"bitbucket.org/ramazan-acar-getir/golang-samples/msg"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 9000))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := msg.Server{}

	grpcServer := grpc.NewServer()

	msg.RegisterMsgServiceServer(grpcServer, &s)

	//evans icin
	reflection.Register(grpcServer)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}