package models

import "fmt"

// var name     string
// var age      int
// var location string

//2. yontem
// var (
// 	name     string
// 	age      int
// 	location string
// )

//3.yontem
// var (
// 	name, location  string
// 	age             int
// )

//direkt deger atama
// var (
// 	name     string = "Prince Oberyn"
// 	age      int    =  32
// 	location string = "Dorne"
// )

//deger atama 2. yontem
// var (
// 	name, location, age = "Prince Oberyn", "Dorne", 32
// )

//kisa (short) deger atama
func main() {
	name, location := "Prince Oberyn", "Dorne"
	age := 32

	//tiplerini yazdiriyoruz
	fmt.Printf("name = %T\n", name)
    fmt.Printf("age = %T\n", age)
    fmt.Printf("location = %T\n", location)
	fmt.Printf("%s (%d) of %s", name, age, location)
}


// func main() {
// 	name, location = "Prince Oberyn", "Dorne"
// 	age = 32

// 	//tiplerini yazdiriyoruz
// 	fmt.Printf("name = %T\n", name)
//     fmt.Printf("age = %T\n", age)
//     fmt.Printf("location = %T\n", location)

// 	//degerleri yazdiriyoruz
// 	fmt.Printf("%s (%d) of %s", name, age, location)
// }
