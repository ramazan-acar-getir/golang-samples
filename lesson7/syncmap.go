package lesson7

import (
	"fmt"
	"sync"
	"time"
)

type SafeCounter2 struct {
	v  sync.Map
}

// Inc increments the counter for the given key.
func (c *SafeCounter2) Inc(key string) {

	if val, ok := c.v.Load(key); ok {
		value := val.(int)
		value++
		c.v.Store(key, value)
	}

}

// Value returns the current value of the counter for the given key.
func (c *SafeCounter2) Value(key string) int {
	if val, ok := c.v.Load(key); ok {
		return val.(int)
	}
	return 0
}

func RunSyncMapCodes()  {
	var sm sync.Map

	sm.Store("somekey", 0)

	c := SafeCounter2{v: sm}

	for i := 0; i < 1000; i++ {
		go c.Inc("somekey")
	}

	time.Sleep(time.Second*5)
	fmt.Println(c.Value("somekey"))
}
