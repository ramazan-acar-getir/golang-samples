package lesson4

import "fmt"

const pi = 3.14

type Geometry interface {
	CalcPerimeter() float32
	CalcArea() float32
	Move(direction string) string
}

type Square struct {
	Width  float32
	Height float32
}

func (s Square) CalcPerimeter() float32 {
	return (s.Width + s.Height) * 2
}

func (s Square) CalcArea() float32 {
	return s.Width * s.Height
}

func (s Square) Move(direction string) string {
	return "square moving" + direction
}

func (s Square) Move2(direction string) string {
	return "square moving" + direction
}

//func (s *Square) CalcPerimeter() float32 {
//	return (s.Width + s.Height) * 2
//}
//
//func (s *Square) CalcArea() float32 {
//	return s.Width * s.Height
//}
//
//func (s *Square) Move(direction string) string {
//	return "square moving" + direction
//}

type Circle struct {
	Radius float32
}

func (c *Circle) CalcPerimeter() float32 {
	return c.Radius * 2 * pi
}

func (c Circle) CalcArea() float32 {
	return pi * c.Radius * c.Radius
}

func (c Circle) Move(direction string) string {
	return "square moving" + direction
}

func RunInterfaceCodes() {
	//shapes := []Geometry{Square{10, 5}, &Circle{3}}
	//circle := &Circle{10}
	s := &Square{10,10}
	//for _, a := range shapes {
	//	fmt.Println(a.CalcArea())
	//	fmt.Println(a.CalcPerimeter())
	//	fmt.Println(a.Move("left"))
	//}

	DoIt(s)
	//shapeList := []interface{}{shapes[0], shapes[1]}
	//DoIt(shapeList)
}

func DoIt(geo Geometry) {
		fmt.Println(geo.CalcArea())
		fmt.Println(geo.CalcPerimeter())
		fmt.Println(geo.Move("left"))
}

//func DoIt(objects []interface{}) {
//	for _, obj := range objects {
//		if object, ok := obj.(Geometry); ok {
//			fmt.Println(object.CalcArea())
//			fmt.Println(object.CalcPerimeter())
//			fmt.Println(object.Move("left"))
//		}
//	}
//}
