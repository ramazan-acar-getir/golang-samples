import grpc

# import the generated classes
import msg_pb2
import msg_pb2_grpc

# open a gRPC channel
channel = grpc.insecure_channel('localhost:9000')

# create a stub (client)
stub = msg_pb2_grpc.MsgServiceStub(channel)

# create a valid request message
hello = msg_pb2.Message(body="Hello From Python")

# make the call
response = stub.SayHello(hello)

# et voilà
print(response.body)