package lesson3

type User struct {
	Id       int
	Name     string
	Location string
}

type Player struct {
	User User
	GameId	 int
}

//type Event struct {
//	Id         int
//	Name       string
//}
//
//func (e Event) GetName() string {
//	return e.Name
//}
//
//func (e *Event) ChangeName(s string) {
//	e.Name = s
//}

func RunStructCode() {

	//x := &Player{}
	//x.User.Location = "x"
	//z := &User{}
	//x.User = *z
	//y := &Player{}
	//x.Name = "hasan"
	//fmt.Println(*x == *y)

	//p := Player{}
	//p.Id = 42
	//p.Name = "Matt"
	//p.Location = "LA"
	//p.GameId = 90404
	//fmt.Printf("%+v", p)

	//e1 := Event{Id: 1, Name: "event 1"}
	//e2 := &Event{Id: 1, Name: "event1"}
	//e1.GetName()
	//e1.ChangeName("test")
	//fmt.Printf("%+v", e1)
	//e2.GetName()
	//e2.ChangeName("test2")
	//fmt.Printf("%+v", e2)

	//vp := ValuePair{
	//	First:  1,
	//	Second: 2,
	//}
	//fmt.Println(vp.add())
	//fmt.Println(vp)
	//fmt.Println(vp.addAnother(1))
	//fmt.Println(vp)
}
