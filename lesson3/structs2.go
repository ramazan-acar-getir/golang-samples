package lesson3

type ValuePair struct {
	First  int
	Second int
	Third  int
}

func (v ValuePair) add() int {
	v.First = 3
	return v.First + v.Second
}

func (v *ValuePair) addAnother(a int) int {
	v.Third = v.First + v.Second + a
	return v.Third
}
