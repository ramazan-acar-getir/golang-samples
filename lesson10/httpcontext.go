package lesson10

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

func RunHttpContextCode() {
	req, err := http.NewRequest("GET", "https://www.getir.com", nil)
	if err != nil {
		log.Fatalf("%v", err)
	}

	ctx, cancel := context.WithTimeout(req.Context(), 1*time.Second)
	defer cancel()

	req = req.WithContext(ctx)

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("%v", err)
	}

	fmt.Printf("%v\n", res.StatusCode)
}